package com.ninja;

import org.junit.Test;

import static junit.framework.TestCase.*;
import static org.junit.Assert.assertFalse;

public class TestPerson {

    //test object Identity
    @Test
    public void sameIdentity() throws Exception
    {
        Person p3 = new Person("Rahmah","de Villiers",5,20.5,false);
        Person p4 = p3;
        assertSame(p3,p4);
    }

    //test equality
    @Test
    public void sameSame() throws Exception
    {
        Person p5 = new Person("Muhammad Nur","de Villiers",0,1.5,true);
        Person p6 = new Person("Muhammad Nur","de Villiers",0,1.5,true);
        assertTrue(Person.checkEquality(p5,p6));
    }

    //test Truth and False
    @Test
    public void trueLies() throws Exception
    {
        Person p7 = new Person("Abu-Bakr","Ahmad",2,10.35,true);
        assertTrue(p7.isMale());

        p7.setMale(false);
        assertFalse(p7.isMale());
    }

    //test NULL & NOTNULL
    @Test
    public void FullOfNothing() throws Exception
    {
        Person p8 = new Person();
        assertNotNull(p8);

        p8 = null;
        assertNull(p8);

    }

    //Failing Test
    @Test
    public void failing() throws Exception
    {
        Person p1 = new Person("Ihsaan","de Villiers",26,50.5,true);
        Person p2 = new Person("Not Ihsaan","NOT de Villiers",26,50.5,true);

        assertSame(p1,p2);

    }

    //TimeOut 
    @Test(timeout = 500)
    public void timeoutTest() {
        while(true);
    }

    
}
