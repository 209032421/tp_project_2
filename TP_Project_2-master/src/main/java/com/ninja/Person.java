package com.ninja;

public class Person {
    private String name;
    private String surname;
    private int age;
    private double weight;
    private boolean isMale;


//Getters and Setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public boolean isMale() {
        return isMale;
    }

    public void setMale(boolean male) {
        isMale = male;
    }

    //Constructor
    public Person() {
    }

    public Person(String name, String surname, int age, double weight, boolean isMale) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.weight = weight;
        this.isMale = isMale;
    }

    //Methods To help with the annotation testing

    public void incrementAge()
    {
        this.age++;
    }

    public void pickedUpWeight()
    {
        this.weight += 1.50;
    }

    public static boolean checkEquality(Person p1, Person p2)
    {
        boolean isEqual = false;

        if(p1.getName() != p2.getName())
        {
            isEqual=false;
        }
        else
        {
            if(p1.getSurname() != p2.getSurname())
            {
                isEqual=false;
            }
            else if(p1.getAge()!= p2.getAge())
            {
                isEqual=false;
            }
            else if(p1.getWeight() != p2.getWeight())
            {
                isEqual=false;
            }
            else if (p1.isMale() != p2.isMale())
            {
                isEqual=false;
            }
            else
            {
                isEqual=true;
            }
        }
        return isEqual;
    }

    public String toString()
    {
        return("Name: \t"+name+"\nSurname: \t"+surname+"\nAge: \t"+age+"\nWeight: \t"+weight+"\nGender: \t"+isMale+"\n");
    }


}
